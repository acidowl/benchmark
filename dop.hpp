#include "util.hpp"

struct AimingData {
    Vec3 positions[COUNT];
    float mod[COUNT];
};

void updateAims(float *aimDir, AimingData *aim, Vec3 target) {
    for (int i = 0; i < COUNT; i++) {
        aimDir[i] = dot3(aim->positions[i], target) * aim->mod[i];
    }
}