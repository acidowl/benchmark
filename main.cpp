#include <cstdlib>
#include <ctime>
#include <chrono>
#include <iostream>
#include "dop.hpp"
#include "oop.hpp"
#include "util.hpp"

void flushCache() {
    srand(time(NULL));
    // this should not flush the instruction cache but the data cache
    // this should be greater than the cache of my machine (Intel i7 5500U with 4MB cache)
    const size_t size = 5 * 1024 * 1024;
    long *p = new long[size];

    for (int i = 0; i < size; i++) {
        // rand() is needed here so the compiler doesn't optimise the call away
        p[i] = rand();
    }
}

int main() {
    Bot bots[COUNT];
    Vec3 targetOOP = generateRandomVec3();

    std::cout << "size of 1 Bot    : " << sizeof(Bot) << std::endl;
    std::cout << "size of all bots : " << sizeof(bots) << std::endl;

    auto t1_oop = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < COUNT; i++) {
        bots[i].updateAim(targetOOP);
    }
    auto t2_oop = std::chrono::high_resolution_clock::now();

    auto duration_oop = std::chrono::duration_cast<std::chrono::nanoseconds>(t2_oop - t1_oop).count();
    std::cout << "OOP execution duration: " << duration_oop << " nanoseconds" << std::endl;

    flushCache();

    float aimDirs[COUNT];
    AimingData aimingData{};
    for (int i = 0; i < COUNT; i++) {
        aimingData.positions[i] = generateRandomVec3();
        aimingData.mod[i] = generateRandomFloat();
    }
    Vec3 targetDOP = generateRandomVec3();

    std::cout << "size of AimingData: " << sizeof(aimingData) << std::endl;

    auto t1_dop = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < COUNT; i++) {
        aimDirs[i] = dot3(aimingData.positions[i], targetDOP) * aimingData.mod[i];
    }
    auto t2_dop = std::chrono::high_resolution_clock::now();

    auto duration_dop = std::chrono::duration_cast<std::chrono::nanoseconds>(t2_dop - t1_dop).count();
    std::cout << "DOP execution duration: " << duration_dop << " nanoseconds" << std::endl;

    return 0;
}