#include <algorithm>
#include "util.hpp"

class Bot {
public:
    Vec3 m_position;
    float m_mod;
    float m_aimDirection;
    float m_a;
    float m_b;
    float m_c;
    float m_d;
    float m_e;

    Bot() {
        m_position = generateRandomVec3();
        m_mod = generateRandomFloat();
        m_a = generateRandomFloat();
        m_b = generateRandomFloat();
        m_c = generateRandomFloat();
        m_d = generateRandomFloat();
        m_e = generateRandomFloat();
    }

    void updateAim(Vec3 target) {
        m_aimDirection = dot3(m_position, target) * m_mod;
    }
};