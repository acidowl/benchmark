#pragma once

#include <numeric>
#include <random>

#define COUNT 5000

struct Vec3 {
    float x;
    float y;
    float z;
};

int generateRandomInt() {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(1, 100);

    return dist(mt);
}

Vec3 generateRandomVec3() {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> intDist(1, 100);

    Vec3 v;
    v.x = intDist(mt);
    v.y = intDist(mt);
    v.z = intDist(mt);

    return v;
}

float generateRandomFloat() {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<float> floatDist(0.0, 1.0);
    return floatDist(mt);
}

float dot3(Vec3 a, Vec3 b) {
    return a.x * b.x + a.y * b.y + a.z * b.z;
}